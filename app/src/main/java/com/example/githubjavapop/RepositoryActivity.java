package com.example.githubjavapop;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.http.HttpResponseCache;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;


public class RepositoryActivity extends Activity {

    ListView repositoryList;
    RepositoryAdapter repositoryAdapter;
    RepositoryLoader repositoryLoader;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repository);

        setTitle("Repositórios");

        context = this;

        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if(!(activeNetworkInfo != null && activeNetworkInfo.isConnected())){
            Intent noNetworkActivity = new Intent(context, NoNetworkActivity.class);
            startActivity(noNetworkActivity);
        }

        repositoryAdapter = new RepositoryAdapter(this);
        repositoryLoader = new RepositoryLoader(this, repositoryAdapter);
        repositoryList = (ListView) findViewById(R.id.listRepository);
        repositoryList.setAdapter(repositoryAdapter);
        repositoryList.setOnScrollListener(new LimitlessScrollListener() {
            @Override
            public void onLoadMore() {
                repositoryLoader.loadResponses();
            }
        });

        repositoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String username = repositoryAdapter.getUsername(position);
                String repository = repositoryAdapter.getRepository(position);

                Intent pullRequestIntent = new Intent(context, PullRequestActivity.class);
                pullRequestIntent.putExtra("username", username);
                pullRequestIntent.putExtra("repository", repository);

                startActivity(pullRequestIntent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
