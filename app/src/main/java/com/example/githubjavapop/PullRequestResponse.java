package com.example.githubjavapop;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PullRequestResponse {

    /**
     * url : https://api.github.com/repos/elastic/elasticsearch/pulls/15671
     * id : 54599130
     * html_url : https://github.com/elastic/elasticsearch/pull/15671
     * diff_url : https://github.com/elastic/elasticsearch/pull/15671.diff
     * patch_url : https://github.com/elastic/elasticsearch/pull/15671.patch
     * issue_url : https://api.github.com/repos/elastic/elasticsearch/issues/15671
     * number : 15671
     * state : open
     * locked : false
     * title : Analyze: Specify custom char_filters/tokenizer/token_filters in the analyze API
     * user : {"login":"johtani","id":1142449,"avatar_url":"https://avatars.githubusercontent.com/u/1142449?v=3","gravatar_id":"","url":"https://api.github.com/users/johtani","html_url":"https://github.com/johtani","followers_url":"https://api.github.com/users/johtani/followers","following_url":"https://api.github.com/users/johtani/following{/other_user}","gists_url":"https://api.github.com/users/johtani/gists{/gist_id}","starred_url":"https://api.github.com/users/johtani/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/johtani/subscriptions","organizations_url":"https://api.github.com/users/johtani/orgs","repos_url":"https://api.github.com/users/johtani/repos","events_url":"https://api.github.com/users/johtani/events{/privacy}","received_events_url":"https://api.github.com/users/johtani/received_events","type":"User","site_admin":false}
     * body : Add parser for custom char_filters/tokenizer/token_filters in analyze API request.

     You don't need to create temporary index to analyze a text with new char_filter/tokenizer/token_filter settings.
     Example: 
     ```
     curl -XGET 'localhost:9200/_analyze' -d '
     {
     "tokenizer" : "whitespace",
     "filters" : ["lowercase", {"type": "stop", "stopwords": ["a", "is", "this"]}],
     "text" : "this is a test"
     }'
     ```

     Closed #8878
     * created_at : 2015-12-26T08:34:18Z
     * updated_at : 2015-12-26T08:34:18Z
     * closed_at : null
     * merged_at : null
     * merge_commit_sha : eabdc5354c3034cca59a018296bea2f47d7427c0
     * assignee : null
     * milestone : null
     * commits_url : https://api.github.com/repos/elastic/elasticsearch/pulls/15671/commits
     * review_comments_url : https://api.github.com/repos/elastic/elasticsearch/pulls/15671/comments
     * review_comment_url : https://api.github.com/repos/elastic/elasticsearch/pulls/comments{/number}
     * comments_url : https://api.github.com/repos/elastic/elasticsearch/issues/15671/comments
     * statuses_url : https://api.github.com/repos/elastic/elasticsearch/statuses/15ef0a0aee14240924266f7223e5b4a3ce4fe255
     * head : {"label":"johtani:fix/support_custom_analyzer_with_analyze_api","ref":"fix/support_custom_analyzer_with_analyze_api","sha":"15ef0a0aee14240924266f7223e5b4a3ce4fe255","user":{"login":"johtani","id":1142449,"avatar_url":"https://avatars.githubusercontent.com/u/1142449?v=3","gravatar_id":"","url":"https://api.github.com/users/johtani","html_url":"https://github.com/johtani","followers_url":"https://api.github.com/users/johtani/followers","following_url":"https://api.github.com/users/johtani/following{/other_user}","gists_url":"https://api.github.com/users/johtani/gists{/gist_id}","starred_url":"https://api.github.com/users/johtani/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/johtani/subscriptions","organizations_url":"https://api.github.com/users/johtani/orgs","repos_url":"https://api.github.com/users/johtani/repos","events_url":"https://api.github.com/users/johtani/events{/privacy}","received_events_url":"https://api.github.com/users/johtani/received_events","type":"User","site_admin":false},"repo":{"id":12916853,"name":"elasticsearch","full_name":"johtani/elasticsearch","owner":{"login":"johtani","id":1142449,"avatar_url":"https://avatars.githubusercontent.com/u/1142449?v=3","gravatar_id":"","url":"https://api.github.com/users/johtani","html_url":"https://github.com/johtani","followers_url":"https://api.github.com/users/johtani/followers","following_url":"https://api.github.com/users/johtani/following{/other_user}","gists_url":"https://api.github.com/users/johtani/gists{/gist_id}","starred_url":"https://api.github.com/users/johtani/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/johtani/subscriptions","organizations_url":"https://api.github.com/users/johtani/orgs","repos_url":"https://api.github.com/users/johtani/repos","events_url":"https://api.github.com/users/johtani/events{/privacy}","received_events_url":"https://api.github.com/users/johtani/received_events","type":"User","site_admin":false},"private":false,"html_url":"https://github.com/johtani/elasticsearch","description":"Open Source, Distributed, RESTful Search Engine","fork":true,"url":"https://api.github.com/repos/johtani/elasticsearch","forks_url":"https://api.github.com/repos/johtani/elasticsearch/forks","keys_url":"https://api.github.com/repos/johtani/elasticsearch/keys{/key_id}","collaborators_url":"https://api.github.com/repos/johtani/elasticsearch/collaborators{/collaborator}","teams_url":"https://api.github.com/repos/johtani/elasticsearch/teams","hooks_url":"https://api.github.com/repos/johtani/elasticsearch/hooks","issue_events_url":"https://api.github.com/repos/johtani/elasticsearch/issues/events{/number}","events_url":"https://api.github.com/repos/johtani/elasticsearch/events","assignees_url":"https://api.github.com/repos/johtani/elasticsearch/assignees{/user}","branches_url":"https://api.github.com/repos/johtani/elasticsearch/branches{/branch}","tags_url":"https://api.github.com/repos/johtani/elasticsearch/tags","blobs_url":"https://api.github.com/repos/johtani/elasticsearch/git/blobs{/sha}","git_tags_url":"https://api.github.com/repos/johtani/elasticsearch/git/tags{/sha}","git_refs_url":"https://api.github.com/repos/johtani/elasticsearch/git/refs{/sha}","trees_url":"https://api.github.com/repos/johtani/elasticsearch/git/trees{/sha}","statuses_url":"https://api.github.com/repos/johtani/elasticsearch/statuses/{sha}","languages_url":"https://api.github.com/repos/johtani/elasticsearch/languages","stargazers_url":"https://api.github.com/repos/johtani/elasticsearch/stargazers","contributors_url":"https://api.github.com/repos/johtani/elasticsearch/contributors","subscribers_url":"https://api.github.com/repos/johtani/elasticsearch/subscribers","subscription_url":"https://api.github.com/repos/johtani/elasticsearch/subscription","commits_url":"https://api.github.com/repos/johtani/elasticsearch/commits{/sha}","git_commits_url":"https://api.github.com/repos/johtani/elasticsearch/git/commits{/sha}","comments_url":"https://api.github.com/repos/johtani/elasticsearch/comments{/number}","issue_comment_url":"https://api.github.com/repos/johtani/elasticsearch/issues/comments{/number}","contents_url":"https://api.github.com/repos/johtani/elasticsearch/contents/{+path}","compare_url":"https://api.github.com/repos/johtani/elasticsearch/compare/{base}...{head}","merges_url":"https://api.github.com/repos/johtani/elasticsearch/merges","archive_url":"https://api.github.com/repos/johtani/elasticsearch/{archive_format}{/ref}","downloads_url":"https://api.github.com/repos/johtani/elasticsearch/downloads","issues_url":"https://api.github.com/repos/johtani/elasticsearch/issues{/number}","pulls_url":"https://api.github.com/repos/johtani/elasticsearch/pulls{/number}","milestones_url":"https://api.github.com/repos/johtani/elasticsearch/milestones{/number}","notifications_url":"https://api.github.com/repos/johtani/elasticsearch/notifications{?since,all,participating}","labels_url":"https://api.github.com/repos/johtani/elasticsearch/labels{/name}","releases_url":"https://api.github.com/repos/johtani/elasticsearch/releases{/id}","created_at":"2013-09-18T07:31:27Z","updated_at":"2014-07-02T06:17:57Z","pushed_at":"2015-12-26T08:27:05Z","git_url":"git://github.com/johtani/elasticsearch.git","ssh_url":"git@github.com:johtani/elasticsearch.git","clone_url":"https://github.com/johtani/elasticsearch.git","svn_url":"https://github.com/johtani/elasticsearch","homepage":"http://elasticsearch.org","size":160594,"stargazers_count":0,"watchers_count":0,"language":"Java","has_issues":false,"has_downloads":true,"has_wiki":false,"has_pages":false,"forks_count":0,"mirror_url":null,"open_issues_count":0,"forks":0,"open_issues":0,"watchers":0,"default_branch":"master"}}
     * base : {"label":"elastic:master","ref":"master","sha":"485915bbe780949a03a1bff0dcde6a81a39de3bb","user":{"login":"elastic","id":6764390,"avatar_url":"https://avatars.githubusercontent.com/u/6764390?v=3","gravatar_id":"","url":"https://api.github.com/users/elastic","html_url":"https://github.com/elastic","followers_url":"https://api.github.com/users/elastic/followers","following_url":"https://api.github.com/users/elastic/following{/other_user}","gists_url":"https://api.github.com/users/elastic/gists{/gist_id}","starred_url":"https://api.github.com/users/elastic/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/elastic/subscriptions","organizations_url":"https://api.github.com/users/elastic/orgs","repos_url":"https://api.github.com/users/elastic/repos","events_url":"https://api.github.com/users/elastic/events{/privacy}","received_events_url":"https://api.github.com/users/elastic/received_events","type":"Organization","site_admin":false},"repo":{"id":507775,"name":"elasticsearch","full_name":"elastic/elasticsearch","owner":{"login":"elastic","id":6764390,"avatar_url":"https://avatars.githubusercontent.com/u/6764390?v=3","gravatar_id":"","url":"https://api.github.com/users/elastic","html_url":"https://github.com/elastic","followers_url":"https://api.github.com/users/elastic/followers","following_url":"https://api.github.com/users/elastic/following{/other_user}","gists_url":"https://api.github.com/users/elastic/gists{/gist_id}","starred_url":"https://api.github.com/users/elastic/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/elastic/subscriptions","organizations_url":"https://api.github.com/users/elastic/orgs","repos_url":"https://api.github.com/users/elastic/repos","events_url":"https://api.github.com/users/elastic/events{/privacy}","received_events_url":"https://api.github.com/users/elastic/received_events","type":"Organization","site_admin":false},"private":false,"html_url":"https://github.com/elastic/elasticsearch","description":"Open Source, Distributed, RESTful Search Engine","fork":false,"url":"https://api.github.com/repos/elastic/elasticsearch","forks_url":"https://api.github.com/repos/elastic/elasticsearch/forks","keys_url":"https://api.github.com/repos/elastic/elasticsearch/keys{/key_id}","collaborators_url":"https://api.github.com/repos/elastic/elasticsearch/collaborators{/collaborator}","teams_url":"https://api.github.com/repos/elastic/elasticsearch/teams","hooks_url":"https://api.github.com/repos/elastic/elasticsearch/hooks","issue_events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/events{/number}","events_url":"https://api.github.com/repos/elastic/elasticsearch/events","assignees_url":"https://api.github.com/repos/elastic/elasticsearch/assignees{/user}","branches_url":"https://api.github.com/repos/elastic/elasticsearch/branches{/branch}","tags_url":"https://api.github.com/repos/elastic/elasticsearch/tags","blobs_url":"https://api.github.com/repos/elastic/elasticsearch/git/blobs{/sha}","git_tags_url":"https://api.github.com/repos/elastic/elasticsearch/git/tags{/sha}","git_refs_url":"https://api.github.com/repos/elastic/elasticsearch/git/refs{/sha}","trees_url":"https://api.github.com/repos/elastic/elasticsearch/git/trees{/sha}","statuses_url":"https://api.github.com/repos/elastic/elasticsearch/statuses/{sha}","languages_url":"https://api.github.com/repos/elastic/elasticsearch/languages","stargazers_url":"https://api.github.com/repos/elastic/elasticsearch/stargazers","contributors_url":"https://api.github.com/repos/elastic/elasticsearch/contributors","subscribers_url":"https://api.github.com/repos/elastic/elasticsearch/subscribers","subscription_url":"https://api.github.com/repos/elastic/elasticsearch/subscription","commits_url":"https://api.github.com/repos/elastic/elasticsearch/commits{/sha}","git_commits_url":"https://api.github.com/repos/elastic/elasticsearch/git/commits{/sha}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/comments{/number}","issue_comment_url":"https://api.github.com/repos/elastic/elasticsearch/issues/comments{/number}","contents_url":"https://api.github.com/repos/elastic/elasticsearch/contents/{+path}","compare_url":"https://api.github.com/repos/elastic/elasticsearch/compare/{base}...{head}","merges_url":"https://api.github.com/repos/elastic/elasticsearch/merges","archive_url":"https://api.github.com/repos/elastic/elasticsearch/{archive_format}{/ref}","downloads_url":"https://api.github.com/repos/elastic/elasticsearch/downloads","issues_url":"https://api.github.com/repos/elastic/elasticsearch/issues{/number}","pulls_url":"https://api.github.com/repos/elastic/elasticsearch/pulls{/number}","milestones_url":"https://api.github.com/repos/elastic/elasticsearch/milestones{/number}","notifications_url":"https://api.github.com/repos/elastic/elasticsearch/notifications{?since,all,participating}","labels_url":"https://api.github.com/repos/elastic/elasticsearch/labels{/name}","releases_url":"https://api.github.com/repos/elastic/elasticsearch/releases{/id}","created_at":"2010-02-08T13:20:56Z","updated_at":"2015-12-26T12:49:00Z","pushed_at":"2015-12-26T08:34:18Z","git_url":"git://github.com/elastic/elasticsearch.git","ssh_url":"git@github.com:elastic/elasticsearch.git","clone_url":"https://github.com/elastic/elasticsearch.git","svn_url":"https://github.com/elastic/elasticsearch","homepage":"https://www.elastic.co/products/elasticsearch","size":232461,"stargazers_count":14129,"watchers_count":14129,"language":"Java","has_issues":true,"has_downloads":true,"has_wiki":false,"has_pages":false,"forks_count":4711,"mirror_url":null,"open_issues_count":1238,"forks":4711,"open_issues":1238,"watchers":14129,"default_branch":"master"}}
     * _links : {"self":{"href":"https://api.github.com/repos/elastic/elasticsearch/pulls/15671"},"html":{"href":"https://github.com/elastic/elasticsearch/pull/15671"},"issue":{"href":"https://api.github.com/repos/elastic/elasticsearch/issues/15671"},"comments":{"href":"https://api.github.com/repos/elastic/elasticsearch/issues/15671/comments"},"review_comments":{"href":"https://api.github.com/repos/elastic/elasticsearch/pulls/15671/comments"},"review_comment":{"href":"https://api.github.com/repos/elastic/elasticsearch/pulls/comments{/number}"},"commits":{"href":"https://api.github.com/repos/elastic/elasticsearch/pulls/15671/commits"},"statuses":{"href":"https://api.github.com/repos/elastic/elasticsearch/statuses/15ef0a0aee14240924266f7223e5b4a3ce4fe255"}}
     */

    private List<ItemsEntity> items;

    public void setItems(List<ItemsEntity> items) {
        this.items = items;
    }

    public List<ItemsEntity> getItems() {
        return items;
    }

    public static class ItemsEntity {
        private String url;
        private int id;
        private String html_url;
        private String diff_url;
        private String patch_url;
        private String issue_url;
        private int number;
        private String state;
        private boolean locked;
        private String title;
        /**
         * login : johtani
         * id : 1142449
         * avatar_url : https://avatars.githubusercontent.com/u/1142449?v=3
         * gravatar_id : 
         * url : https://api.github.com/users/johtani
         * html_url : https://github.com/johtani
         * followers_url : https://api.github.com/users/johtani/followers
         * following_url : https://api.github.com/users/johtani/following{/other_user}
         * gists_url : https://api.github.com/users/johtani/gists{/gist_id}
         * starred_url : https://api.github.com/users/johtani/starred{/owner}{/repo}
         * subscriptions_url : https://api.github.com/users/johtani/subscriptions
         * organizations_url : https://api.github.com/users/johtani/orgs
         * repos_url : https://api.github.com/users/johtani/repos
         * events_url : https://api.github.com/users/johtani/events{/privacy}
         * received_events_url : https://api.github.com/users/johtani/received_events
         * type : User
         * site_admin : false
         */

        private UserEntity user;
        private String body;
        private String created_at;
        private String updated_at;
        private Object closed_at;
        private Object merged_at;
        private String merge_commit_sha;
        private Object assignee;
        private Object milestone;
        private String commits_url;
        private String review_comments_url;
        private String review_comment_url;
        private String comments_url;
        private String statuses_url;
        /**
         * label : johtani:fix/support_custom_analyzer_with_analyze_api
         * ref : fix/support_custom_analyzer_with_analyze_api
         * sha : 15ef0a0aee14240924266f7223e5b4a3ce4fe255
         * user : {"login":"johtani","id":1142449,"avatar_url":"https://avatars.githubusercontent.com/u/1142449?v=3","gravatar_id":"","url":"https://api.github.com/users/johtani","html_url":"https://github.com/johtani","followers_url":"https://api.github.com/users/johtani/followers","following_url":"https://api.github.com/users/johtani/following{/other_user}","gists_url":"https://api.github.com/users/johtani/gists{/gist_id}","starred_url":"https://api.github.com/users/johtani/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/johtani/subscriptions","organizations_url":"https://api.github.com/users/johtani/orgs","repos_url":"https://api.github.com/users/johtani/repos","events_url":"https://api.github.com/users/johtani/events{/privacy}","received_events_url":"https://api.github.com/users/johtani/received_events","type":"User","site_admin":false}
         * repo : {"id":12916853,"name":"elasticsearch","full_name":"johtani/elasticsearch","owner":{"login":"johtani","id":1142449,"avatar_url":"https://avatars.githubusercontent.com/u/1142449?v=3","gravatar_id":"","url":"https://api.github.com/users/johtani","html_url":"https://github.com/johtani","followers_url":"https://api.github.com/users/johtani/followers","following_url":"https://api.github.com/users/johtani/following{/other_user}","gists_url":"https://api.github.com/users/johtani/gists{/gist_id}","starred_url":"https://api.github.com/users/johtani/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/johtani/subscriptions","organizations_url":"https://api.github.com/users/johtani/orgs","repos_url":"https://api.github.com/users/johtani/repos","events_url":"https://api.github.com/users/johtani/events{/privacy}","received_events_url":"https://api.github.com/users/johtani/received_events","type":"User","site_admin":false},"private":false,"html_url":"https://github.com/johtani/elasticsearch","description":"Open Source, Distributed, RESTful Search Engine","fork":true,"url":"https://api.github.com/repos/johtani/elasticsearch","forks_url":"https://api.github.com/repos/johtani/elasticsearch/forks","keys_url":"https://api.github.com/repos/johtani/elasticsearch/keys{/key_id}","collaborators_url":"https://api.github.com/repos/johtani/elasticsearch/collaborators{/collaborator}","teams_url":"https://api.github.com/repos/johtani/elasticsearch/teams","hooks_url":"https://api.github.com/repos/johtani/elasticsearch/hooks","issue_events_url":"https://api.github.com/repos/johtani/elasticsearch/issues/events{/number}","events_url":"https://api.github.com/repos/johtani/elasticsearch/events","assignees_url":"https://api.github.com/repos/johtani/elasticsearch/assignees{/user}","branches_url":"https://api.github.com/repos/johtani/elasticsearch/branches{/branch}","tags_url":"https://api.github.com/repos/johtani/elasticsearch/tags","blobs_url":"https://api.github.com/repos/johtani/elasticsearch/git/blobs{/sha}","git_tags_url":"https://api.github.com/repos/johtani/elasticsearch/git/tags{/sha}","git_refs_url":"https://api.github.com/repos/johtani/elasticsearch/git/refs{/sha}","trees_url":"https://api.github.com/repos/johtani/elasticsearch/git/trees{/sha}","statuses_url":"https://api.github.com/repos/johtani/elasticsearch/statuses/{sha}","languages_url":"https://api.github.com/repos/johtani/elasticsearch/languages","stargazers_url":"https://api.github.com/repos/johtani/elasticsearch/stargazers","contributors_url":"https://api.github.com/repos/johtani/elasticsearch/contributors","subscribers_url":"https://api.github.com/repos/johtani/elasticsearch/subscribers","subscription_url":"https://api.github.com/repos/johtani/elasticsearch/subscription","commits_url":"https://api.github.com/repos/johtani/elasticsearch/commits{/sha}","git_commits_url":"https://api.github.com/repos/johtani/elasticsearch/git/commits{/sha}","comments_url":"https://api.github.com/repos/johtani/elasticsearch/comments{/number}","issue_comment_url":"https://api.github.com/repos/johtani/elasticsearch/issues/comments{/number}","contents_url":"https://api.github.com/repos/johtani/elasticsearch/contents/{+path}","compare_url":"https://api.github.com/repos/johtani/elasticsearch/compare/{base}...{head}","merges_url":"https://api.github.com/repos/johtani/elasticsearch/merges","archive_url":"https://api.github.com/repos/johtani/elasticsearch/{archive_format}{/ref}","downloads_url":"https://api.github.com/repos/johtani/elasticsearch/downloads","issues_url":"https://api.github.com/repos/johtani/elasticsearch/issues{/number}","pulls_url":"https://api.github.com/repos/johtani/elasticsearch/pulls{/number}","milestones_url":"https://api.github.com/repos/johtani/elasticsearch/milestones{/number}","notifications_url":"https://api.github.com/repos/johtani/elasticsearch/notifications{?since,all,participating}","labels_url":"https://api.github.com/repos/johtani/elasticsearch/labels{/name}","releases_url":"https://api.github.com/repos/johtani/elasticsearch/releases{/id}","created_at":"2013-09-18T07:31:27Z","updated_at":"2014-07-02T06:17:57Z","pushed_at":"2015-12-26T08:27:05Z","git_url":"git://github.com/johtani/elasticsearch.git","ssh_url":"git@github.com:johtani/elasticsearch.git","clone_url":"https://github.com/johtani/elasticsearch.git","svn_url":"https://github.com/johtani/elasticsearch","homepage":"http://elasticsearch.org","size":160594,"stargazers_count":0,"watchers_count":0,"language":"Java","has_issues":false,"has_downloads":true,"has_wiki":false,"has_pages":false,"forks_count":0,"mirror_url":null,"open_issues_count":0,"forks":0,"open_issues":0,"watchers":0,"default_branch":"master"}
         */

        private HeadEntity head;
        /**
         * label : elastic:master
         * ref : master
         * sha : 485915bbe780949a03a1bff0dcde6a81a39de3bb
         * user : {"login":"elastic","id":6764390,"avatar_url":"https://avatars.githubusercontent.com/u/6764390?v=3","gravatar_id":"","url":"https://api.github.com/users/elastic","html_url":"https://github.com/elastic","followers_url":"https://api.github.com/users/elastic/followers","following_url":"https://api.github.com/users/elastic/following{/other_user}","gists_url":"https://api.github.com/users/elastic/gists{/gist_id}","starred_url":"https://api.github.com/users/elastic/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/elastic/subscriptions","organizations_url":"https://api.github.com/users/elastic/orgs","repos_url":"https://api.github.com/users/elastic/repos","events_url":"https://api.github.com/users/elastic/events{/privacy}","received_events_url":"https://api.github.com/users/elastic/received_events","type":"Organization","site_admin":false}
         * repo : {"id":507775,"name":"elasticsearch","full_name":"elastic/elasticsearch","owner":{"login":"elastic","id":6764390,"avatar_url":"https://avatars.githubusercontent.com/u/6764390?v=3","gravatar_id":"","url":"https://api.github.com/users/elastic","html_url":"https://github.com/elastic","followers_url":"https://api.github.com/users/elastic/followers","following_url":"https://api.github.com/users/elastic/following{/other_user}","gists_url":"https://api.github.com/users/elastic/gists{/gist_id}","starred_url":"https://api.github.com/users/elastic/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/elastic/subscriptions","organizations_url":"https://api.github.com/users/elastic/orgs","repos_url":"https://api.github.com/users/elastic/repos","events_url":"https://api.github.com/users/elastic/events{/privacy}","received_events_url":"https://api.github.com/users/elastic/received_events","type":"Organization","site_admin":false},"private":false,"html_url":"https://github.com/elastic/elasticsearch","description":"Open Source, Distributed, RESTful Search Engine","fork":false,"url":"https://api.github.com/repos/elastic/elasticsearch","forks_url":"https://api.github.com/repos/elastic/elasticsearch/forks","keys_url":"https://api.github.com/repos/elastic/elasticsearch/keys{/key_id}","collaborators_url":"https://api.github.com/repos/elastic/elasticsearch/collaborators{/collaborator}","teams_url":"https://api.github.com/repos/elastic/elasticsearch/teams","hooks_url":"https://api.github.com/repos/elastic/elasticsearch/hooks","issue_events_url":"https://api.github.com/repos/elastic/elasticsearch/issues/events{/number}","events_url":"https://api.github.com/repos/elastic/elasticsearch/events","assignees_url":"https://api.github.com/repos/elastic/elasticsearch/assignees{/user}","branches_url":"https://api.github.com/repos/elastic/elasticsearch/branches{/branch}","tags_url":"https://api.github.com/repos/elastic/elasticsearch/tags","blobs_url":"https://api.github.com/repos/elastic/elasticsearch/git/blobs{/sha}","git_tags_url":"https://api.github.com/repos/elastic/elasticsearch/git/tags{/sha}","git_refs_url":"https://api.github.com/repos/elastic/elasticsearch/git/refs{/sha}","trees_url":"https://api.github.com/repos/elastic/elasticsearch/git/trees{/sha}","statuses_url":"https://api.github.com/repos/elastic/elasticsearch/statuses/{sha}","languages_url":"https://api.github.com/repos/elastic/elasticsearch/languages","stargazers_url":"https://api.github.com/repos/elastic/elasticsearch/stargazers","contributors_url":"https://api.github.com/repos/elastic/elasticsearch/contributors","subscribers_url":"https://api.github.com/repos/elastic/elasticsearch/subscribers","subscription_url":"https://api.github.com/repos/elastic/elasticsearch/subscription","commits_url":"https://api.github.com/repos/elastic/elasticsearch/commits{/sha}","git_commits_url":"https://api.github.com/repos/elastic/elasticsearch/git/commits{/sha}","comments_url":"https://api.github.com/repos/elastic/elasticsearch/comments{/number}","issue_comment_url":"https://api.github.com/repos/elastic/elasticsearch/issues/comments{/number}","contents_url":"https://api.github.com/repos/elastic/elasticsearch/contents/{+path}","compare_url":"https://api.github.com/repos/elastic/elasticsearch/compare/{base}...{head}","merges_url":"https://api.github.com/repos/elastic/elasticsearch/merges","archive_url":"https://api.github.com/repos/elastic/elasticsearch/{archive_format}{/ref}","downloads_url":"https://api.github.com/repos/elastic/elasticsearch/downloads","issues_url":"https://api.github.com/repos/elastic/elasticsearch/issues{/number}","pulls_url":"https://api.github.com/repos/elastic/elasticsearch/pulls{/number}","milestones_url":"https://api.github.com/repos/elastic/elasticsearch/milestones{/number}","notifications_url":"https://api.github.com/repos/elastic/elasticsearch/notifications{?since,all,participating}","labels_url":"https://api.github.com/repos/elastic/elasticsearch/labels{/name}","releases_url":"https://api.github.com/repos/elastic/elasticsearch/releases{/id}","created_at":"2010-02-08T13:20:56Z","updated_at":"2015-12-26T12:49:00Z","pushed_at":"2015-12-26T08:34:18Z","git_url":"git://github.com/elastic/elasticsearch.git","ssh_url":"git@github.com:elastic/elasticsearch.git","clone_url":"https://github.com/elastic/elasticsearch.git","svn_url":"https://github.com/elastic/elasticsearch","homepage":"https://www.elastic.co/products/elasticsearch","size":232461,"stargazers_count":14129,"watchers_count":14129,"language":"Java","has_issues":true,"has_downloads":true,"has_wiki":false,"has_pages":false,"forks_count":4711,"mirror_url":null,"open_issues_count":1238,"forks":4711,"open_issues":1238,"watchers":14129,"default_branch":"master"}
         */

        private BaseEntity base;
        /**
         * self : {"href":"https://api.github.com/repos/elastic/elasticsearch/pulls/15671"}
         * html : {"href":"https://github.com/elastic/elasticsearch/pull/15671"}
         * issue : {"href":"https://api.github.com/repos/elastic/elasticsearch/issues/15671"}
         * comments : {"href":"https://api.github.com/repos/elastic/elasticsearch/issues/15671/comments"}
         * review_comments : {"href":"https://api.github.com/repos/elastic/elasticsearch/pulls/15671/comments"}
         * review_comment : {"href":"https://api.github.com/repos/elastic/elasticsearch/pulls/comments{/number}"}
         * commits : {"href":"https://api.github.com/repos/elastic/elasticsearch/pulls/15671/commits"}
         * statuses : {"href":"https://api.github.com/repos/elastic/elasticsearch/statuses/15ef0a0aee14240924266f7223e5b4a3ce4fe255"}
         */

        private LinksEntity _links;

        public void setUrl(String url) {
            this.url = url;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setHtml_url(String html_url) {
            this.html_url = html_url;
        }

        public void setDiff_url(String diff_url) {
            this.diff_url = diff_url;
        }

        public void setPatch_url(String patch_url) {
            this.patch_url = patch_url;
        }

        public void setIssue_url(String issue_url) {
            this.issue_url = issue_url;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public void setState(String state) {
            this.state = state;
        }

        public void setLocked(boolean locked) {
            this.locked = locked;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setUser(UserEntity user) {
            this.user = user;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public void setClosed_at(Object closed_at) {
            this.closed_at = closed_at;
        }

        public void setMerged_at(Object merged_at) {
            this.merged_at = merged_at;
        }

        public void setMerge_commit_sha(String merge_commit_sha) {
            this.merge_commit_sha = merge_commit_sha;
        }

        public void setAssignee(Object assignee) {
            this.assignee = assignee;
        }

        public void setMilestone(Object milestone) {
            this.milestone = milestone;
        }

        public void setCommits_url(String commits_url) {
            this.commits_url = commits_url;
        }

        public void setReview_comments_url(String review_comments_url) {
            this.review_comments_url = review_comments_url;
        }

        public void setReview_comment_url(String review_comment_url) {
            this.review_comment_url = review_comment_url;
        }

        public void setComments_url(String comments_url) {
            this.comments_url = comments_url;
        }

        public void setStatuses_url(String statuses_url) {
            this.statuses_url = statuses_url;
        }

        public void setHead(HeadEntity head) {
            this.head = head;
        }

        public void setBase(BaseEntity base) {
            this.base = base;
        }

        public void set_links(LinksEntity _links) {
            this._links = _links;
        }

        public String getUrl() {
            return url;
        }

        public int getId() {
            return id;
        }

        public String getHtml_url() {
            return html_url;
        }

        public String getDiff_url() {
            return diff_url;
        }

        public String getPatch_url() {
            return patch_url;
        }

        public String getIssue_url() {
            return issue_url;
        }

        public int getNumber() {
            return number;
        }

        public String getState() {
            return state;
        }

        public boolean isLocked() {
            return locked;
        }

        public String getTitle() {
            return title;
        }

        public UserEntity getUser() {
            return user;
        }

        public String getBody() {
            return body;
        }

        public String getCreated_at() {
            return created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public Object getClosed_at() {
            return closed_at;
        }

        public Object getMerged_at() {
            return merged_at;
        }

        public String getMerge_commit_sha() {
            return merge_commit_sha;
        }

        public Object getAssignee() {
            return assignee;
        }

        public Object getMilestone() {
            return milestone;
        }

        public String getCommits_url() {
            return commits_url;
        }

        public String getReview_comments_url() {
            return review_comments_url;
        }

        public String getReview_comment_url() {
            return review_comment_url;
        }

        public String getComments_url() {
            return comments_url;
        }

        public String getStatuses_url() {
            return statuses_url;
        }

        public HeadEntity getHead() {
            return head;
        }

        public BaseEntity getBase() {
            return base;
        }

        public LinksEntity get_links() {
            return _links;
        }

        public static class UserEntity {
            private String login;
            private int id;
            private String avatar_url;
            private String gravatar_id;
            private String url;
            private String html_url;
            private String followers_url;
            private String following_url;
            private String gists_url;
            private String starred_url;
            private String subscriptions_url;
            private String organizations_url;
            private String repos_url;
            private String events_url;
            private String received_events_url;
            private String type;
            private boolean site_admin;

            public void setLogin(String login) {
                this.login = login;
            }

            public void setId(int id) {
                this.id = id;
            }

            public void setAvatar_url(String avatar_url) {
                this.avatar_url = avatar_url;
            }

            public void setGravatar_id(String gravatar_id) {
                this.gravatar_id = gravatar_id;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public void setHtml_url(String html_url) {
                this.html_url = html_url;
            }

            public void setFollowers_url(String followers_url) {
                this.followers_url = followers_url;
            }

            public void setFollowing_url(String following_url) {
                this.following_url = following_url;
            }

            public void setGists_url(String gists_url) {
                this.gists_url = gists_url;
            }

            public void setStarred_url(String starred_url) {
                this.starred_url = starred_url;
            }

            public void setSubscriptions_url(String subscriptions_url) {
                this.subscriptions_url = subscriptions_url;
            }

            public void setOrganizations_url(String organizations_url) {
                this.organizations_url = organizations_url;
            }

            public void setRepos_url(String repos_url) {
                this.repos_url = repos_url;
            }

            public void setEvents_url(String events_url) {
                this.events_url = events_url;
            }

            public void setReceived_events_url(String received_events_url) {
                this.received_events_url = received_events_url;
            }

            public void setType(String type) {
                this.type = type;
            }

            public void setSite_admin(boolean site_admin) {
                this.site_admin = site_admin;
            }

            public String getLogin() {
                return login;
            }

            public int getId() {
                return id;
            }

            public String getAvatar_url() {
                return avatar_url;
            }

            public String getGravatar_id() {
                return gravatar_id;
            }

            public String getUrl() {
                return url;
            }

            public String getHtml_url() {
                return html_url;
            }

            public String getFollowers_url() {
                return followers_url;
            }

            public String getFollowing_url() {
                return following_url;
            }

            public String getGists_url() {
                return gists_url;
            }

            public String getStarred_url() {
                return starred_url;
            }

            public String getSubscriptions_url() {
                return subscriptions_url;
            }

            public String getOrganizations_url() {
                return organizations_url;
            }

            public String getRepos_url() {
                return repos_url;
            }

            public String getEvents_url() {
                return events_url;
            }

            public String getReceived_events_url() {
                return received_events_url;
            }

            public String getType() {
                return type;
            }

            public boolean isSite_admin() {
                return site_admin;
            }
        }

        public static class HeadEntity {
            private String label;
            private String ref;
            private String sha;
            /**
             * login : johtani
             * id : 1142449
             * avatar_url : https://avatars.githubusercontent.com/u/1142449?v=3
             * gravatar_id : 
             * url : https://api.github.com/users/johtani
             * html_url : https://github.com/johtani
             * followers_url : https://api.github.com/users/johtani/followers
             * following_url : https://api.github.com/users/johtani/following{/other_user}
             * gists_url : https://api.github.com/users/johtani/gists{/gist_id}
             * starred_url : https://api.github.com/users/johtani/starred{/owner}{/repo}
             * subscriptions_url : https://api.github.com/users/johtani/subscriptions
             * organizations_url : https://api.github.com/users/johtani/orgs
             * repos_url : https://api.github.com/users/johtani/repos
             * events_url : https://api.github.com/users/johtani/events{/privacy}
             * received_events_url : https://api.github.com/users/johtani/received_events
             * type : User
             * site_admin : false
             */

            private UserEntity user;
            /**
             * id : 12916853
             * name : elasticsearch
             * full_name : johtani/elasticsearch
             * owner : {"login":"johtani","id":1142449,"avatar_url":"https://avatars.githubusercontent.com/u/1142449?v=3","gravatar_id":"","url":"https://api.github.com/users/johtani","html_url":"https://github.com/johtani","followers_url":"https://api.github.com/users/johtani/followers","following_url":"https://api.github.com/users/johtani/following{/other_user}","gists_url":"https://api.github.com/users/johtani/gists{/gist_id}","starred_url":"https://api.github.com/users/johtani/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/johtani/subscriptions","organizations_url":"https://api.github.com/users/johtani/orgs","repos_url":"https://api.github.com/users/johtani/repos","events_url":"https://api.github.com/users/johtani/events{/privacy}","received_events_url":"https://api.github.com/users/johtani/received_events","type":"User","site_admin":false}
             * private : false
             * html_url : https://github.com/johtani/elasticsearch
             * description : Open Source, Distributed, RESTful Search Engine
             * fork : true
             * url : https://api.github.com/repos/johtani/elasticsearch
             * forks_url : https://api.github.com/repos/johtani/elasticsearch/forks
             * keys_url : https://api.github.com/repos/johtani/elasticsearch/keys{/key_id}
             * collaborators_url : https://api.github.com/repos/johtani/elasticsearch/collaborators{/collaborator}
             * teams_url : https://api.github.com/repos/johtani/elasticsearch/teams
             * hooks_url : https://api.github.com/repos/johtani/elasticsearch/hooks
             * issue_events_url : https://api.github.com/repos/johtani/elasticsearch/issues/events{/number}
             * events_url : https://api.github.com/repos/johtani/elasticsearch/events
             * assignees_url : https://api.github.com/repos/johtani/elasticsearch/assignees{/user}
             * branches_url : https://api.github.com/repos/johtani/elasticsearch/branches{/branch}
             * tags_url : https://api.github.com/repos/johtani/elasticsearch/tags
             * blobs_url : https://api.github.com/repos/johtani/elasticsearch/git/blobs{/sha}
             * git_tags_url : https://api.github.com/repos/johtani/elasticsearch/git/tags{/sha}
             * git_refs_url : https://api.github.com/repos/johtani/elasticsearch/git/refs{/sha}
             * trees_url : https://api.github.com/repos/johtani/elasticsearch/git/trees{/sha}
             * statuses_url : https://api.github.com/repos/johtani/elasticsearch/statuses/{sha}
             * languages_url : https://api.github.com/repos/johtani/elasticsearch/languages
             * stargazers_url : https://api.github.com/repos/johtani/elasticsearch/stargazers
             * contributors_url : https://api.github.com/repos/johtani/elasticsearch/contributors
             * subscribers_url : https://api.github.com/repos/johtani/elasticsearch/subscribers
             * subscription_url : https://api.github.com/repos/johtani/elasticsearch/subscription
             * commits_url : https://api.github.com/repos/johtani/elasticsearch/commits{/sha}
             * git_commits_url : https://api.github.com/repos/johtani/elasticsearch/git/commits{/sha}
             * comments_url : https://api.github.com/repos/johtani/elasticsearch/comments{/number}
             * issue_comment_url : https://api.github.com/repos/johtani/elasticsearch/issues/comments{/number}
             * contents_url : https://api.github.com/repos/johtani/elasticsearch/contents/{+path}
             * compare_url : https://api.github.com/repos/johtani/elasticsearch/compare/{base}...{head}
             * merges_url : https://api.github.com/repos/johtani/elasticsearch/merges
             * archive_url : https://api.github.com/repos/johtani/elasticsearch/{archive_format}{/ref}
             * downloads_url : https://api.github.com/repos/johtani/elasticsearch/downloads
             * issues_url : https://api.github.com/repos/johtani/elasticsearch/issues{/number}
             * pulls_url : https://api.github.com/repos/johtani/elasticsearch/pulls{/number}
             * milestones_url : https://api.github.com/repos/johtani/elasticsearch/milestones{/number}
             * notifications_url : https://api.github.com/repos/johtani/elasticsearch/notifications{?since,all,participating}
             * labels_url : https://api.github.com/repos/johtani/elasticsearch/labels{/name}
             * releases_url : https://api.github.com/repos/johtani/elasticsearch/releases{/id}
             * created_at : 2013-09-18T07:31:27Z
             * updated_at : 2014-07-02T06:17:57Z
             * pushed_at : 2015-12-26T08:27:05Z
             * git_url : git://github.com/johtani/elasticsearch.git
             * ssh_url : git@github.com:johtani/elasticsearch.git
             * clone_url : https://github.com/johtani/elasticsearch.git
             * svn_url : https://github.com/johtani/elasticsearch
             * homepage : http://elasticsearch.org
             * size : 160594
             * stargazers_count : 0
             * watchers_count : 0
             * language : Java
             * has_issues : false
             * has_downloads : true
             * has_wiki : false
             * has_pages : false
             * forks_count : 0
             * mirror_url : null
             * open_issues_count : 0
             * forks : 0
             * open_issues : 0
             * watchers : 0
             * default_branch : master
             */

            private RepoEntity repo;

            public void setLabel(String label) {
                this.label = label;
            }

            public void setRef(String ref) {
                this.ref = ref;
            }

            public void setSha(String sha) {
                this.sha = sha;
            }

            public void setUser(UserEntity user) {
                this.user = user;
            }

            public void setRepo(RepoEntity repo) {
                this.repo = repo;
            }

            public String getLabel() {
                return label;
            }

            public String getRef() {
                return ref;
            }

            public String getSha() {
                return sha;
            }

            public UserEntity getUser() {
                return user;
            }

            public RepoEntity getRepo() {
                return repo;
            }

            public static class UserEntity {
                private String login;
                private int id;
                private String avatar_url;
                private String gravatar_id;
                private String url;
                private String html_url;
                private String followers_url;
                private String following_url;
                private String gists_url;
                private String starred_url;
                private String subscriptions_url;
                private String organizations_url;
                private String repos_url;
                private String events_url;
                private String received_events_url;
                private String type;
                private boolean site_admin;

                public void setLogin(String login) {
                    this.login = login;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public void setAvatar_url(String avatar_url) {
                    this.avatar_url = avatar_url;
                }

                public void setGravatar_id(String gravatar_id) {
                    this.gravatar_id = gravatar_id;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public void setHtml_url(String html_url) {
                    this.html_url = html_url;
                }

                public void setFollowers_url(String followers_url) {
                    this.followers_url = followers_url;
                }

                public void setFollowing_url(String following_url) {
                    this.following_url = following_url;
                }

                public void setGists_url(String gists_url) {
                    this.gists_url = gists_url;
                }

                public void setStarred_url(String starred_url) {
                    this.starred_url = starred_url;
                }

                public void setSubscriptions_url(String subscriptions_url) {
                    this.subscriptions_url = subscriptions_url;
                }

                public void setOrganizations_url(String organizations_url) {
                    this.organizations_url = organizations_url;
                }

                public void setRepos_url(String repos_url) {
                    this.repos_url = repos_url;
                }

                public void setEvents_url(String events_url) {
                    this.events_url = events_url;
                }

                public void setReceived_events_url(String received_events_url) {
                    this.received_events_url = received_events_url;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public void setSite_admin(boolean site_admin) {
                    this.site_admin = site_admin;
                }

                public String getLogin() {
                    return login;
                }

                public int getId() {
                    return id;
                }

                public String getAvatar_url() {
                    return avatar_url;
                }

                public String getGravatar_id() {
                    return gravatar_id;
                }

                public String getUrl() {
                    return url;
                }

                public String getHtml_url() {
                    return html_url;
                }

                public String getFollowers_url() {
                    return followers_url;
                }

                public String getFollowing_url() {
                    return following_url;
                }

                public String getGists_url() {
                    return gists_url;
                }

                public String getStarred_url() {
                    return starred_url;
                }

                public String getSubscriptions_url() {
                    return subscriptions_url;
                }

                public String getOrganizations_url() {
                    return organizations_url;
                }

                public String getRepos_url() {
                    return repos_url;
                }

                public String getEvents_url() {
                    return events_url;
                }

                public String getReceived_events_url() {
                    return received_events_url;
                }

                public String getType() {
                    return type;
                }

                public boolean isSite_admin() {
                    return site_admin;
                }
            }

            public static class RepoEntity {
                private int id;
                private String name;
                private String full_name;
                /**
                 * login : johtani
                 * id : 1142449
                 * avatar_url : https://avatars.githubusercontent.com/u/1142449?v=3
                 * gravatar_id : 
                 * url : https://api.github.com/users/johtani
                 * html_url : https://github.com/johtani
                 * followers_url : https://api.github.com/users/johtani/followers
                 * following_url : https://api.github.com/users/johtani/following{/other_user}
                 * gists_url : https://api.github.com/users/johtani/gists{/gist_id}
                 * starred_url : https://api.github.com/users/johtani/starred{/owner}{/repo}
                 * subscriptions_url : https://api.github.com/users/johtani/subscriptions
                 * organizations_url : https://api.github.com/users/johtani/orgs
                 * repos_url : https://api.github.com/users/johtani/repos
                 * events_url : https://api.github.com/users/johtani/events{/privacy}
                 * received_events_url : https://api.github.com/users/johtani/received_events
                 * type : User
                 * site_admin : false
                 */

                private OwnerEntity owner;
                @SerializedName("private")
                private boolean privateX;
                private String html_url;
                private String description;
                private boolean fork;
                private String url;
                private String forks_url;
                private String keys_url;
                private String collaborators_url;
                private String teams_url;
                private String hooks_url;
                private String issue_events_url;
                private String events_url;
                private String assignees_url;
                private String branches_url;
                private String tags_url;
                private String blobs_url;
                private String git_tags_url;
                private String git_refs_url;
                private String trees_url;
                private String statuses_url;
                private String languages_url;
                private String stargazers_url;
                private String contributors_url;
                private String subscribers_url;
                private String subscription_url;
                private String commits_url;
                private String git_commits_url;
                private String comments_url;
                private String issue_comment_url;
                private String contents_url;
                private String compare_url;
                private String merges_url;
                private String archive_url;
                private String downloads_url;
                private String issues_url;
                private String pulls_url;
                private String milestones_url;
                private String notifications_url;
                private String labels_url;
                private String releases_url;
                private String created_at;
                private String updated_at;
                private String pushed_at;
                private String git_url;
                private String ssh_url;
                private String clone_url;
                private String svn_url;
                private String homepage;
                private int size;
                private int stargazers_count;
                private int watchers_count;
                private String language;
                private boolean has_issues;
                private boolean has_downloads;
                private boolean has_wiki;
                private boolean has_pages;
                private int forks_count;
                private Object mirror_url;
                private int open_issues_count;
                private int forks;
                private int open_issues;
                private int watchers;
                private String default_branch;

                public void setId(int id) {
                    this.id = id;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public void setFull_name(String full_name) {
                    this.full_name = full_name;
                }

                public void setOwner(OwnerEntity owner) {
                    this.owner = owner;
                }

                public void setPrivateX(boolean privateX) {
                    this.privateX = privateX;
                }

                public void setHtml_url(String html_url) {
                    this.html_url = html_url;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public void setFork(boolean fork) {
                    this.fork = fork;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public void setForks_url(String forks_url) {
                    this.forks_url = forks_url;
                }

                public void setKeys_url(String keys_url) {
                    this.keys_url = keys_url;
                }

                public void setCollaborators_url(String collaborators_url) {
                    this.collaborators_url = collaborators_url;
                }

                public void setTeams_url(String teams_url) {
                    this.teams_url = teams_url;
                }

                public void setHooks_url(String hooks_url) {
                    this.hooks_url = hooks_url;
                }

                public void setIssue_events_url(String issue_events_url) {
                    this.issue_events_url = issue_events_url;
                }

                public void setEvents_url(String events_url) {
                    this.events_url = events_url;
                }

                public void setAssignees_url(String assignees_url) {
                    this.assignees_url = assignees_url;
                }

                public void setBranches_url(String branches_url) {
                    this.branches_url = branches_url;
                }

                public void setTags_url(String tags_url) {
                    this.tags_url = tags_url;
                }

                public void setBlobs_url(String blobs_url) {
                    this.blobs_url = blobs_url;
                }

                public void setGit_tags_url(String git_tags_url) {
                    this.git_tags_url = git_tags_url;
                }

                public void setGit_refs_url(String git_refs_url) {
                    this.git_refs_url = git_refs_url;
                }

                public void setTrees_url(String trees_url) {
                    this.trees_url = trees_url;
                }

                public void setStatuses_url(String statuses_url) {
                    this.statuses_url = statuses_url;
                }

                public void setLanguages_url(String languages_url) {
                    this.languages_url = languages_url;
                }

                public void setStargazers_url(String stargazers_url) {
                    this.stargazers_url = stargazers_url;
                }

                public void setContributors_url(String contributors_url) {
                    this.contributors_url = contributors_url;
                }

                public void setSubscribers_url(String subscribers_url) {
                    this.subscribers_url = subscribers_url;
                }

                public void setSubscription_url(String subscription_url) {
                    this.subscription_url = subscription_url;
                }

                public void setCommits_url(String commits_url) {
                    this.commits_url = commits_url;
                }

                public void setGit_commits_url(String git_commits_url) {
                    this.git_commits_url = git_commits_url;
                }

                public void setComments_url(String comments_url) {
                    this.comments_url = comments_url;
                }

                public void setIssue_comment_url(String issue_comment_url) {
                    this.issue_comment_url = issue_comment_url;
                }

                public void setContents_url(String contents_url) {
                    this.contents_url = contents_url;
                }

                public void setCompare_url(String compare_url) {
                    this.compare_url = compare_url;
                }

                public void setMerges_url(String merges_url) {
                    this.merges_url = merges_url;
                }

                public void setArchive_url(String archive_url) {
                    this.archive_url = archive_url;
                }

                public void setDownloads_url(String downloads_url) {
                    this.downloads_url = downloads_url;
                }

                public void setIssues_url(String issues_url) {
                    this.issues_url = issues_url;
                }

                public void setPulls_url(String pulls_url) {
                    this.pulls_url = pulls_url;
                }

                public void setMilestones_url(String milestones_url) {
                    this.milestones_url = milestones_url;
                }

                public void setNotifications_url(String notifications_url) {
                    this.notifications_url = notifications_url;
                }

                public void setLabels_url(String labels_url) {
                    this.labels_url = labels_url;
                }

                public void setReleases_url(String releases_url) {
                    this.releases_url = releases_url;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public void setUpdated_at(String updated_at) {
                    this.updated_at = updated_at;
                }

                public void setPushed_at(String pushed_at) {
                    this.pushed_at = pushed_at;
                }

                public void setGit_url(String git_url) {
                    this.git_url = git_url;
                }

                public void setSsh_url(String ssh_url) {
                    this.ssh_url = ssh_url;
                }

                public void setClone_url(String clone_url) {
                    this.clone_url = clone_url;
                }

                public void setSvn_url(String svn_url) {
                    this.svn_url = svn_url;
                }

                public void setHomepage(String homepage) {
                    this.homepage = homepage;
                }

                public void setSize(int size) {
                    this.size = size;
                }

                public void setStargazers_count(int stargazers_count) {
                    this.stargazers_count = stargazers_count;
                }

                public void setWatchers_count(int watchers_count) {
                    this.watchers_count = watchers_count;
                }

                public void setLanguage(String language) {
                    this.language = language;
                }

                public void setHas_issues(boolean has_issues) {
                    this.has_issues = has_issues;
                }

                public void setHas_downloads(boolean has_downloads) {
                    this.has_downloads = has_downloads;
                }

                public void setHas_wiki(boolean has_wiki) {
                    this.has_wiki = has_wiki;
                }

                public void setHas_pages(boolean has_pages) {
                    this.has_pages = has_pages;
                }

                public void setForks_count(int forks_count) {
                    this.forks_count = forks_count;
                }

                public void setMirror_url(Object mirror_url) {
                    this.mirror_url = mirror_url;
                }

                public void setOpen_issues_count(int open_issues_count) {
                    this.open_issues_count = open_issues_count;
                }

                public void setForks(int forks) {
                    this.forks = forks;
                }

                public void setOpen_issues(int open_issues) {
                    this.open_issues = open_issues;
                }

                public void setWatchers(int watchers) {
                    this.watchers = watchers;
                }

                public void setDefault_branch(String default_branch) {
                    this.default_branch = default_branch;
                }

                public int getId() {
                    return id;
                }

                public String getName() {
                    return name;
                }

                public String getFull_name() {
                    return full_name;
                }

                public OwnerEntity getOwner() {
                    return owner;
                }

                public boolean isPrivateX() {
                    return privateX;
                }

                public String getHtml_url() {
                    return html_url;
                }

                public String getDescription() {
                    return description;
                }

                public boolean isFork() {
                    return fork;
                }

                public String getUrl() {
                    return url;
                }

                public String getForks_url() {
                    return forks_url;
                }

                public String getKeys_url() {
                    return keys_url;
                }

                public String getCollaborators_url() {
                    return collaborators_url;
                }

                public String getTeams_url() {
                    return teams_url;
                }

                public String getHooks_url() {
                    return hooks_url;
                }

                public String getIssue_events_url() {
                    return issue_events_url;
                }

                public String getEvents_url() {
                    return events_url;
                }

                public String getAssignees_url() {
                    return assignees_url;
                }

                public String getBranches_url() {
                    return branches_url;
                }

                public String getTags_url() {
                    return tags_url;
                }

                public String getBlobs_url() {
                    return blobs_url;
                }

                public String getGit_tags_url() {
                    return git_tags_url;
                }

                public String getGit_refs_url() {
                    return git_refs_url;
                }

                public String getTrees_url() {
                    return trees_url;
                }

                public String getStatuses_url() {
                    return statuses_url;
                }

                public String getLanguages_url() {
                    return languages_url;
                }

                public String getStargazers_url() {
                    return stargazers_url;
                }

                public String getContributors_url() {
                    return contributors_url;
                }

                public String getSubscribers_url() {
                    return subscribers_url;
                }

                public String getSubscription_url() {
                    return subscription_url;
                }

                public String getCommits_url() {
                    return commits_url;
                }

                public String getGit_commits_url() {
                    return git_commits_url;
                }

                public String getComments_url() {
                    return comments_url;
                }

                public String getIssue_comment_url() {
                    return issue_comment_url;
                }

                public String getContents_url() {
                    return contents_url;
                }

                public String getCompare_url() {
                    return compare_url;
                }

                public String getMerges_url() {
                    return merges_url;
                }

                public String getArchive_url() {
                    return archive_url;
                }

                public String getDownloads_url() {
                    return downloads_url;
                }

                public String getIssues_url() {
                    return issues_url;
                }

                public String getPulls_url() {
                    return pulls_url;
                }

                public String getMilestones_url() {
                    return milestones_url;
                }

                public String getNotifications_url() {
                    return notifications_url;
                }

                public String getLabels_url() {
                    return labels_url;
                }

                public String getReleases_url() {
                    return releases_url;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public String getUpdated_at() {
                    return updated_at;
                }

                public String getPushed_at() {
                    return pushed_at;
                }

                public String getGit_url() {
                    return git_url;
                }

                public String getSsh_url() {
                    return ssh_url;
                }

                public String getClone_url() {
                    return clone_url;
                }

                public String getSvn_url() {
                    return svn_url;
                }

                public String getHomepage() {
                    return homepage;
                }

                public int getSize() {
                    return size;
                }

                public int getStargazers_count() {
                    return stargazers_count;
                }

                public int getWatchers_count() {
                    return watchers_count;
                }

                public String getLanguage() {
                    return language;
                }

                public boolean isHas_issues() {
                    return has_issues;
                }

                public boolean isHas_downloads() {
                    return has_downloads;
                }

                public boolean isHas_wiki() {
                    return has_wiki;
                }

                public boolean isHas_pages() {
                    return has_pages;
                }

                public int getForks_count() {
                    return forks_count;
                }

                public Object getMirror_url() {
                    return mirror_url;
                }

                public int getOpen_issues_count() {
                    return open_issues_count;
                }

                public int getForks() {
                    return forks;
                }

                public int getOpen_issues() {
                    return open_issues;
                }

                public int getWatchers() {
                    return watchers;
                }

                public String getDefault_branch() {
                    return default_branch;
                }

                public static class OwnerEntity {
                    private String login;
                    private int id;
                    private String avatar_url;
                    private String gravatar_id;
                    private String url;
                    private String html_url;
                    private String followers_url;
                    private String following_url;
                    private String gists_url;
                    private String starred_url;
                    private String subscriptions_url;
                    private String organizations_url;
                    private String repos_url;
                    private String events_url;
                    private String received_events_url;
                    private String type;
                    private boolean site_admin;

                    public void setLogin(String login) {
                        this.login = login;
                    }

                    public void setId(int id) {
                        this.id = id;
                    }

                    public void setAvatar_url(String avatar_url) {
                        this.avatar_url = avatar_url;
                    }

                    public void setGravatar_id(String gravatar_id) {
                        this.gravatar_id = gravatar_id;
                    }

                    public void setUrl(String url) {
                        this.url = url;
                    }

                    public void setHtml_url(String html_url) {
                        this.html_url = html_url;
                    }

                    public void setFollowers_url(String followers_url) {
                        this.followers_url = followers_url;
                    }

                    public void setFollowing_url(String following_url) {
                        this.following_url = following_url;
                    }

                    public void setGists_url(String gists_url) {
                        this.gists_url = gists_url;
                    }

                    public void setStarred_url(String starred_url) {
                        this.starred_url = starred_url;
                    }

                    public void setSubscriptions_url(String subscriptions_url) {
                        this.subscriptions_url = subscriptions_url;
                    }

                    public void setOrganizations_url(String organizations_url) {
                        this.organizations_url = organizations_url;
                    }

                    public void setRepos_url(String repos_url) {
                        this.repos_url = repos_url;
                    }

                    public void setEvents_url(String events_url) {
                        this.events_url = events_url;
                    }

                    public void setReceived_events_url(String received_events_url) {
                        this.received_events_url = received_events_url;
                    }

                    public void setType(String type) {
                        this.type = type;
                    }

                    public void setSite_admin(boolean site_admin) {
                        this.site_admin = site_admin;
                    }

                    public String getLogin() {
                        return login;
                    }

                    public int getId() {
                        return id;
                    }

                    public String getAvatar_url() {
                        return avatar_url;
                    }

                    public String getGravatar_id() {
                        return gravatar_id;
                    }

                    public String getUrl() {
                        return url;
                    }

                    public String getHtml_url() {
                        return html_url;
                    }

                    public String getFollowers_url() {
                        return followers_url;
                    }

                    public String getFollowing_url() {
                        return following_url;
                    }

                    public String getGists_url() {
                        return gists_url;
                    }

                    public String getStarred_url() {
                        return starred_url;
                    }

                    public String getSubscriptions_url() {
                        return subscriptions_url;
                    }

                    public String getOrganizations_url() {
                        return organizations_url;
                    }

                    public String getRepos_url() {
                        return repos_url;
                    }

                    public String getEvents_url() {
                        return events_url;
                    }

                    public String getReceived_events_url() {
                        return received_events_url;
                    }

                    public String getType() {
                        return type;
                    }

                    public boolean isSite_admin() {
                        return site_admin;
                    }
                }
            }
        }

        public static class BaseEntity {
            private String label;
            private String ref;
            private String sha;
            /**
             * login : elastic
             * id : 6764390
             * avatar_url : https://avatars.githubusercontent.com/u/6764390?v=3
             * gravatar_id : 
             * url : https://api.github.com/users/elastic
             * html_url : https://github.com/elastic
             * followers_url : https://api.github.com/users/elastic/followers
             * following_url : https://api.github.com/users/elastic/following{/other_user}
             * gists_url : https://api.github.com/users/elastic/gists{/gist_id}
             * starred_url : https://api.github.com/users/elastic/starred{/owner}{/repo}
             * subscriptions_url : https://api.github.com/users/elastic/subscriptions
             * organizations_url : https://api.github.com/users/elastic/orgs
             * repos_url : https://api.github.com/users/elastic/repos
             * events_url : https://api.github.com/users/elastic/events{/privacy}
             * received_events_url : https://api.github.com/users/elastic/received_events
             * type : Organization
             * site_admin : false
             */

            private UserEntity user;
            /**
             * id : 507775
             * name : elasticsearch
             * full_name : elastic/elasticsearch
             * owner : {"login":"elastic","id":6764390,"avatar_url":"https://avatars.githubusercontent.com/u/6764390?v=3","gravatar_id":"","url":"https://api.github.com/users/elastic","html_url":"https://github.com/elastic","followers_url":"https://api.github.com/users/elastic/followers","following_url":"https://api.github.com/users/elastic/following{/other_user}","gists_url":"https://api.github.com/users/elastic/gists{/gist_id}","starred_url":"https://api.github.com/users/elastic/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/elastic/subscriptions","organizations_url":"https://api.github.com/users/elastic/orgs","repos_url":"https://api.github.com/users/elastic/repos","events_url":"https://api.github.com/users/elastic/events{/privacy}","received_events_url":"https://api.github.com/users/elastic/received_events","type":"Organization","site_admin":false}
             * private : false
             * html_url : https://github.com/elastic/elasticsearch
             * description : Open Source, Distributed, RESTful Search Engine
             * fork : false
             * url : https://api.github.com/repos/elastic/elasticsearch
             * forks_url : https://api.github.com/repos/elastic/elasticsearch/forks
             * keys_url : https://api.github.com/repos/elastic/elasticsearch/keys{/key_id}
             * collaborators_url : https://api.github.com/repos/elastic/elasticsearch/collaborators{/collaborator}
             * teams_url : https://api.github.com/repos/elastic/elasticsearch/teams
             * hooks_url : https://api.github.com/repos/elastic/elasticsearch/hooks
             * issue_events_url : https://api.github.com/repos/elastic/elasticsearch/issues/events{/number}
             * events_url : https://api.github.com/repos/elastic/elasticsearch/events
             * assignees_url : https://api.github.com/repos/elastic/elasticsearch/assignees{/user}
             * branches_url : https://api.github.com/repos/elastic/elasticsearch/branches{/branch}
             * tags_url : https://api.github.com/repos/elastic/elasticsearch/tags
             * blobs_url : https://api.github.com/repos/elastic/elasticsearch/git/blobs{/sha}
             * git_tags_url : https://api.github.com/repos/elastic/elasticsearch/git/tags{/sha}
             * git_refs_url : https://api.github.com/repos/elastic/elasticsearch/git/refs{/sha}
             * trees_url : https://api.github.com/repos/elastic/elasticsearch/git/trees{/sha}
             * statuses_url : https://api.github.com/repos/elastic/elasticsearch/statuses/{sha}
             * languages_url : https://api.github.com/repos/elastic/elasticsearch/languages
             * stargazers_url : https://api.github.com/repos/elastic/elasticsearch/stargazers
             * contributors_url : https://api.github.com/repos/elastic/elasticsearch/contributors
             * subscribers_url : https://api.github.com/repos/elastic/elasticsearch/subscribers
             * subscription_url : https://api.github.com/repos/elastic/elasticsearch/subscription
             * commits_url : https://api.github.com/repos/elastic/elasticsearch/commits{/sha}
             * git_commits_url : https://api.github.com/repos/elastic/elasticsearch/git/commits{/sha}
             * comments_url : https://api.github.com/repos/elastic/elasticsearch/comments{/number}
             * issue_comment_url : https://api.github.com/repos/elastic/elasticsearch/issues/comments{/number}
             * contents_url : https://api.github.com/repos/elastic/elasticsearch/contents/{+path}
             * compare_url : https://api.github.com/repos/elastic/elasticsearch/compare/{base}...{head}
             * merges_url : https://api.github.com/repos/elastic/elasticsearch/merges
             * archive_url : https://api.github.com/repos/elastic/elasticsearch/{archive_format}{/ref}
             * downloads_url : https://api.github.com/repos/elastic/elasticsearch/downloads
             * issues_url : https://api.github.com/repos/elastic/elasticsearch/issues{/number}
             * pulls_url : https://api.github.com/repos/elastic/elasticsearch/pulls{/number}
             * milestones_url : https://api.github.com/repos/elastic/elasticsearch/milestones{/number}
             * notifications_url : https://api.github.com/repos/elastic/elasticsearch/notifications{?since,all,participating}
             * labels_url : https://api.github.com/repos/elastic/elasticsearch/labels{/name}
             * releases_url : https://api.github.com/repos/elastic/elasticsearch/releases{/id}
             * created_at : 2010-02-08T13:20:56Z
             * updated_at : 2015-12-26T12:49:00Z
             * pushed_at : 2015-12-26T08:34:18Z
             * git_url : git://github.com/elastic/elasticsearch.git
             * ssh_url : git@github.com:elastic/elasticsearch.git
             * clone_url : https://github.com/elastic/elasticsearch.git
             * svn_url : https://github.com/elastic/elasticsearch
             * homepage : https://www.elastic.co/products/elasticsearch
             * size : 232461
             * stargazers_count : 14129
             * watchers_count : 14129
             * language : Java
             * has_issues : true
             * has_downloads : true
             * has_wiki : false
             * has_pages : false
             * forks_count : 4711
             * mirror_url : null
             * open_issues_count : 1238
             * forks : 4711
             * open_issues : 1238
             * watchers : 14129
             * default_branch : master
             */

            private RepoEntity repo;

            public void setLabel(String label) {
                this.label = label;
            }

            public void setRef(String ref) {
                this.ref = ref;
            }

            public void setSha(String sha) {
                this.sha = sha;
            }

            public void setUser(UserEntity user) {
                this.user = user;
            }

            public void setRepo(RepoEntity repo) {
                this.repo = repo;
            }

            public String getLabel() {
                return label;
            }

            public String getRef() {
                return ref;
            }

            public String getSha() {
                return sha;
            }

            public UserEntity getUser() {
                return user;
            }

            public RepoEntity getRepo() {
                return repo;
            }

            public static class UserEntity {
                private String login;
                private int id;
                private String avatar_url;
                private String gravatar_id;
                private String url;
                private String html_url;
                private String followers_url;
                private String following_url;
                private String gists_url;
                private String starred_url;
                private String subscriptions_url;
                private String organizations_url;
                private String repos_url;
                private String events_url;
                private String received_events_url;
                private String type;
                private boolean site_admin;

                public void setLogin(String login) {
                    this.login = login;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public void setAvatar_url(String avatar_url) {
                    this.avatar_url = avatar_url;
                }

                public void setGravatar_id(String gravatar_id) {
                    this.gravatar_id = gravatar_id;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public void setHtml_url(String html_url) {
                    this.html_url = html_url;
                }

                public void setFollowers_url(String followers_url) {
                    this.followers_url = followers_url;
                }

                public void setFollowing_url(String following_url) {
                    this.following_url = following_url;
                }

                public void setGists_url(String gists_url) {
                    this.gists_url = gists_url;
                }

                public void setStarred_url(String starred_url) {
                    this.starred_url = starred_url;
                }

                public void setSubscriptions_url(String subscriptions_url) {
                    this.subscriptions_url = subscriptions_url;
                }

                public void setOrganizations_url(String organizations_url) {
                    this.organizations_url = organizations_url;
                }

                public void setRepos_url(String repos_url) {
                    this.repos_url = repos_url;
                }

                public void setEvents_url(String events_url) {
                    this.events_url = events_url;
                }

                public void setReceived_events_url(String received_events_url) {
                    this.received_events_url = received_events_url;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public void setSite_admin(boolean site_admin) {
                    this.site_admin = site_admin;
                }

                public String getLogin() {
                    return login;
                }

                public int getId() {
                    return id;
                }

                public String getAvatar_url() {
                    return avatar_url;
                }

                public String getGravatar_id() {
                    return gravatar_id;
                }

                public String getUrl() {
                    return url;
                }

                public String getHtml_url() {
                    return html_url;
                }

                public String getFollowers_url() {
                    return followers_url;
                }

                public String getFollowing_url() {
                    return following_url;
                }

                public String getGists_url() {
                    return gists_url;
                }

                public String getStarred_url() {
                    return starred_url;
                }

                public String getSubscriptions_url() {
                    return subscriptions_url;
                }

                public String getOrganizations_url() {
                    return organizations_url;
                }

                public String getRepos_url() {
                    return repos_url;
                }

                public String getEvents_url() {
                    return events_url;
                }

                public String getReceived_events_url() {
                    return received_events_url;
                }

                public String getType() {
                    return type;
                }

                public boolean isSite_admin() {
                    return site_admin;
                }
            }

            public static class RepoEntity {
                private int id;
                private String name;
                private String full_name;
                /**
                 * login : elastic
                 * id : 6764390
                 * avatar_url : https://avatars.githubusercontent.com/u/6764390?v=3
                 * gravatar_id : 
                 * url : https://api.github.com/users/elastic
                 * html_url : https://github.com/elastic
                 * followers_url : https://api.github.com/users/elastic/followers
                 * following_url : https://api.github.com/users/elastic/following{/other_user}
                 * gists_url : https://api.github.com/users/elastic/gists{/gist_id}
                 * starred_url : https://api.github.com/users/elastic/starred{/owner}{/repo}
                 * subscriptions_url : https://api.github.com/users/elastic/subscriptions
                 * organizations_url : https://api.github.com/users/elastic/orgs
                 * repos_url : https://api.github.com/users/elastic/repos
                 * events_url : https://api.github.com/users/elastic/events{/privacy}
                 * received_events_url : https://api.github.com/users/elastic/received_events
                 * type : Organization
                 * site_admin : false
                 */

                private OwnerEntity owner;
                @SerializedName("private")
                private boolean privateX;
                private String html_url;
                private String description;
                private boolean fork;
                private String url;
                private String forks_url;
                private String keys_url;
                private String collaborators_url;
                private String teams_url;
                private String hooks_url;
                private String issue_events_url;
                private String events_url;
                private String assignees_url;
                private String branches_url;
                private String tags_url;
                private String blobs_url;
                private String git_tags_url;
                private String git_refs_url;
                private String trees_url;
                private String statuses_url;
                private String languages_url;
                private String stargazers_url;
                private String contributors_url;
                private String subscribers_url;
                private String subscription_url;
                private String commits_url;
                private String git_commits_url;
                private String comments_url;
                private String issue_comment_url;
                private String contents_url;
                private String compare_url;
                private String merges_url;
                private String archive_url;
                private String downloads_url;
                private String issues_url;
                private String pulls_url;
                private String milestones_url;
                private String notifications_url;
                private String labels_url;
                private String releases_url;
                private String created_at;
                private String updated_at;
                private String pushed_at;
                private String git_url;
                private String ssh_url;
                private String clone_url;
                private String svn_url;
                private String homepage;
                private int size;
                private int stargazers_count;
                private int watchers_count;
                private String language;
                private boolean has_issues;
                private boolean has_downloads;
                private boolean has_wiki;
                private boolean has_pages;
                private int forks_count;
                private Object mirror_url;
                private int open_issues_count;
                private int forks;
                private int open_issues;
                private int watchers;
                private String default_branch;

                public void setId(int id) {
                    this.id = id;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public void setFull_name(String full_name) {
                    this.full_name = full_name;
                }

                public void setOwner(OwnerEntity owner) {
                    this.owner = owner;
                }

                public void setPrivateX(boolean privateX) {
                    this.privateX = privateX;
                }

                public void setHtml_url(String html_url) {
                    this.html_url = html_url;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public void setFork(boolean fork) {
                    this.fork = fork;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public void setForks_url(String forks_url) {
                    this.forks_url = forks_url;
                }

                public void setKeys_url(String keys_url) {
                    this.keys_url = keys_url;
                }

                public void setCollaborators_url(String collaborators_url) {
                    this.collaborators_url = collaborators_url;
                }

                public void setTeams_url(String teams_url) {
                    this.teams_url = teams_url;
                }

                public void setHooks_url(String hooks_url) {
                    this.hooks_url = hooks_url;
                }

                public void setIssue_events_url(String issue_events_url) {
                    this.issue_events_url = issue_events_url;
                }

                public void setEvents_url(String events_url) {
                    this.events_url = events_url;
                }

                public void setAssignees_url(String assignees_url) {
                    this.assignees_url = assignees_url;
                }

                public void setBranches_url(String branches_url) {
                    this.branches_url = branches_url;
                }

                public void setTags_url(String tags_url) {
                    this.tags_url = tags_url;
                }

                public void setBlobs_url(String blobs_url) {
                    this.blobs_url = blobs_url;
                }

                public void setGit_tags_url(String git_tags_url) {
                    this.git_tags_url = git_tags_url;
                }

                public void setGit_refs_url(String git_refs_url) {
                    this.git_refs_url = git_refs_url;
                }

                public void setTrees_url(String trees_url) {
                    this.trees_url = trees_url;
                }

                public void setStatuses_url(String statuses_url) {
                    this.statuses_url = statuses_url;
                }

                public void setLanguages_url(String languages_url) {
                    this.languages_url = languages_url;
                }

                public void setStargazers_url(String stargazers_url) {
                    this.stargazers_url = stargazers_url;
                }

                public void setContributors_url(String contributors_url) {
                    this.contributors_url = contributors_url;
                }

                public void setSubscribers_url(String subscribers_url) {
                    this.subscribers_url = subscribers_url;
                }

                public void setSubscription_url(String subscription_url) {
                    this.subscription_url = subscription_url;
                }

                public void setCommits_url(String commits_url) {
                    this.commits_url = commits_url;
                }

                public void setGit_commits_url(String git_commits_url) {
                    this.git_commits_url = git_commits_url;
                }

                public void setComments_url(String comments_url) {
                    this.comments_url = comments_url;
                }

                public void setIssue_comment_url(String issue_comment_url) {
                    this.issue_comment_url = issue_comment_url;
                }

                public void setContents_url(String contents_url) {
                    this.contents_url = contents_url;
                }

                public void setCompare_url(String compare_url) {
                    this.compare_url = compare_url;
                }

                public void setMerges_url(String merges_url) {
                    this.merges_url = merges_url;
                }

                public void setArchive_url(String archive_url) {
                    this.archive_url = archive_url;
                }

                public void setDownloads_url(String downloads_url) {
                    this.downloads_url = downloads_url;
                }

                public void setIssues_url(String issues_url) {
                    this.issues_url = issues_url;
                }

                public void setPulls_url(String pulls_url) {
                    this.pulls_url = pulls_url;
                }

                public void setMilestones_url(String milestones_url) {
                    this.milestones_url = milestones_url;
                }

                public void setNotifications_url(String notifications_url) {
                    this.notifications_url = notifications_url;
                }

                public void setLabels_url(String labels_url) {
                    this.labels_url = labels_url;
                }

                public void setReleases_url(String releases_url) {
                    this.releases_url = releases_url;
                }

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public void setUpdated_at(String updated_at) {
                    this.updated_at = updated_at;
                }

                public void setPushed_at(String pushed_at) {
                    this.pushed_at = pushed_at;
                }

                public void setGit_url(String git_url) {
                    this.git_url = git_url;
                }

                public void setSsh_url(String ssh_url) {
                    this.ssh_url = ssh_url;
                }

                public void setClone_url(String clone_url) {
                    this.clone_url = clone_url;
                }

                public void setSvn_url(String svn_url) {
                    this.svn_url = svn_url;
                }

                public void setHomepage(String homepage) {
                    this.homepage = homepage;
                }

                public void setSize(int size) {
                    this.size = size;
                }

                public void setStargazers_count(int stargazers_count) {
                    this.stargazers_count = stargazers_count;
                }

                public void setWatchers_count(int watchers_count) {
                    this.watchers_count = watchers_count;
                }

                public void setLanguage(String language) {
                    this.language = language;
                }

                public void setHas_issues(boolean has_issues) {
                    this.has_issues = has_issues;
                }

                public void setHas_downloads(boolean has_downloads) {
                    this.has_downloads = has_downloads;
                }

                public void setHas_wiki(boolean has_wiki) {
                    this.has_wiki = has_wiki;
                }

                public void setHas_pages(boolean has_pages) {
                    this.has_pages = has_pages;
                }

                public void setForks_count(int forks_count) {
                    this.forks_count = forks_count;
                }

                public void setMirror_url(Object mirror_url) {
                    this.mirror_url = mirror_url;
                }

                public void setOpen_issues_count(int open_issues_count) {
                    this.open_issues_count = open_issues_count;
                }

                public void setForks(int forks) {
                    this.forks = forks;
                }

                public void setOpen_issues(int open_issues) {
                    this.open_issues = open_issues;
                }

                public void setWatchers(int watchers) {
                    this.watchers = watchers;
                }

                public void setDefault_branch(String default_branch) {
                    this.default_branch = default_branch;
                }

                public int getId() {
                    return id;
                }

                public String getName() {
                    return name;
                }

                public String getFull_name() {
                    return full_name;
                }

                public OwnerEntity getOwner() {
                    return owner;
                }

                public boolean isPrivateX() {
                    return privateX;
                }

                public String getHtml_url() {
                    return html_url;
                }

                public String getDescription() {
                    return description;
                }

                public boolean isFork() {
                    return fork;
                }

                public String getUrl() {
                    return url;
                }

                public String getForks_url() {
                    return forks_url;
                }

                public String getKeys_url() {
                    return keys_url;
                }

                public String getCollaborators_url() {
                    return collaborators_url;
                }

                public String getTeams_url() {
                    return teams_url;
                }

                public String getHooks_url() {
                    return hooks_url;
                }

                public String getIssue_events_url() {
                    return issue_events_url;
                }

                public String getEvents_url() {
                    return events_url;
                }

                public String getAssignees_url() {
                    return assignees_url;
                }

                public String getBranches_url() {
                    return branches_url;
                }

                public String getTags_url() {
                    return tags_url;
                }

                public String getBlobs_url() {
                    return blobs_url;
                }

                public String getGit_tags_url() {
                    return git_tags_url;
                }

                public String getGit_refs_url() {
                    return git_refs_url;
                }

                public String getTrees_url() {
                    return trees_url;
                }

                public String getStatuses_url() {
                    return statuses_url;
                }

                public String getLanguages_url() {
                    return languages_url;
                }

                public String getStargazers_url() {
                    return stargazers_url;
                }

                public String getContributors_url() {
                    return contributors_url;
                }

                public String getSubscribers_url() {
                    return subscribers_url;
                }

                public String getSubscription_url() {
                    return subscription_url;
                }

                public String getCommits_url() {
                    return commits_url;
                }

                public String getGit_commits_url() {
                    return git_commits_url;
                }

                public String getComments_url() {
                    return comments_url;
                }

                public String getIssue_comment_url() {
                    return issue_comment_url;
                }

                public String getContents_url() {
                    return contents_url;
                }

                public String getCompare_url() {
                    return compare_url;
                }

                public String getMerges_url() {
                    return merges_url;
                }

                public String getArchive_url() {
                    return archive_url;
                }

                public String getDownloads_url() {
                    return downloads_url;
                }

                public String getIssues_url() {
                    return issues_url;
                }

                public String getPulls_url() {
                    return pulls_url;
                }

                public String getMilestones_url() {
                    return milestones_url;
                }

                public String getNotifications_url() {
                    return notifications_url;
                }

                public String getLabels_url() {
                    return labels_url;
                }

                public String getReleases_url() {
                    return releases_url;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public String getUpdated_at() {
                    return updated_at;
                }

                public String getPushed_at() {
                    return pushed_at;
                }

                public String getGit_url() {
                    return git_url;
                }

                public String getSsh_url() {
                    return ssh_url;
                }

                public String getClone_url() {
                    return clone_url;
                }

                public String getSvn_url() {
                    return svn_url;
                }

                public String getHomepage() {
                    return homepage;
                }

                public int getSize() {
                    return size;
                }

                public int getStargazers_count() {
                    return stargazers_count;
                }

                public int getWatchers_count() {
                    return watchers_count;
                }

                public String getLanguage() {
                    return language;
                }

                public boolean isHas_issues() {
                    return has_issues;
                }

                public boolean isHas_downloads() {
                    return has_downloads;
                }

                public boolean isHas_wiki() {
                    return has_wiki;
                }

                public boolean isHas_pages() {
                    return has_pages;
                }

                public int getForks_count() {
                    return forks_count;
                }

                public Object getMirror_url() {
                    return mirror_url;
                }

                public int getOpen_issues_count() {
                    return open_issues_count;
                }

                public int getForks() {
                    return forks;
                }

                public int getOpen_issues() {
                    return open_issues;
                }

                public int getWatchers() {
                    return watchers;
                }

                public String getDefault_branch() {
                    return default_branch;
                }

                public static class OwnerEntity {
                    private String login;
                    private int id;
                    private String avatar_url;
                    private String gravatar_id;
                    private String url;
                    private String html_url;
                    private String followers_url;
                    private String following_url;
                    private String gists_url;
                    private String starred_url;
                    private String subscriptions_url;
                    private String organizations_url;
                    private String repos_url;
                    private String events_url;
                    private String received_events_url;
                    private String type;
                    private boolean site_admin;

                    public void setLogin(String login) {
                        this.login = login;
                    }

                    public void setId(int id) {
                        this.id = id;
                    }

                    public void setAvatar_url(String avatar_url) {
                        this.avatar_url = avatar_url;
                    }

                    public void setGravatar_id(String gravatar_id) {
                        this.gravatar_id = gravatar_id;
                    }

                    public void setUrl(String url) {
                        this.url = url;
                    }

                    public void setHtml_url(String html_url) {
                        this.html_url = html_url;
                    }

                    public void setFollowers_url(String followers_url) {
                        this.followers_url = followers_url;
                    }

                    public void setFollowing_url(String following_url) {
                        this.following_url = following_url;
                    }

                    public void setGists_url(String gists_url) {
                        this.gists_url = gists_url;
                    }

                    public void setStarred_url(String starred_url) {
                        this.starred_url = starred_url;
                    }

                    public void setSubscriptions_url(String subscriptions_url) {
                        this.subscriptions_url = subscriptions_url;
                    }

                    public void setOrganizations_url(String organizations_url) {
                        this.organizations_url = organizations_url;
                    }

                    public void setRepos_url(String repos_url) {
                        this.repos_url = repos_url;
                    }

                    public void setEvents_url(String events_url) {
                        this.events_url = events_url;
                    }

                    public void setReceived_events_url(String received_events_url) {
                        this.received_events_url = received_events_url;
                    }

                    public void setType(String type) {
                        this.type = type;
                    }

                    public void setSite_admin(boolean site_admin) {
                        this.site_admin = site_admin;
                    }

                    public String getLogin() {
                        return login;
                    }

                    public int getId() {
                        return id;
                    }

                    public String getAvatar_url() {
                        return avatar_url;
                    }

                    public String getGravatar_id() {
                        return gravatar_id;
                    }

                    public String getUrl() {
                        return url;
                    }

                    public String getHtml_url() {
                        return html_url;
                    }

                    public String getFollowers_url() {
                        return followers_url;
                    }

                    public String getFollowing_url() {
                        return following_url;
                    }

                    public String getGists_url() {
                        return gists_url;
                    }

                    public String getStarred_url() {
                        return starred_url;
                    }

                    public String getSubscriptions_url() {
                        return subscriptions_url;
                    }

                    public String getOrganizations_url() {
                        return organizations_url;
                    }

                    public String getRepos_url() {
                        return repos_url;
                    }

                    public String getEvents_url() {
                        return events_url;
                    }

                    public String getReceived_events_url() {
                        return received_events_url;
                    }

                    public String getType() {
                        return type;
                    }

                    public boolean isSite_admin() {
                        return site_admin;
                    }
                }
            }
        }

        public static class LinksEntity {
            /**
             * href : https://api.github.com/repos/elastic/elasticsearch/pulls/15671
             */

            private SelfEntity self;
            /**
             * href : https://github.com/elastic/elasticsearch/pull/15671
             */

            private HtmlEntity html;
            /**
             * href : https://api.github.com/repos/elastic/elasticsearch/issues/15671
             */

            private IssueEntity issue;
            /**
             * href : https://api.github.com/repos/elastic/elasticsearch/issues/15671/comments
             */

            private CommentsEntity comments;
            /**
             * href : https://api.github.com/repos/elastic/elasticsearch/pulls/15671/comments
             */

            private ReviewCommentsEntity review_comments;
            /**
             * href : https://api.github.com/repos/elastic/elasticsearch/pulls/comments{/number}
             */

            private ReviewCommentEntity review_comment;
            /**
             * href : https://api.github.com/repos/elastic/elasticsearch/pulls/15671/commits
             */

            private CommitsEntity commits;
            /**
             * href : https://api.github.com/repos/elastic/elasticsearch/statuses/15ef0a0aee14240924266f7223e5b4a3ce4fe255
             */

            private StatusesEntity statuses;

            public void setSelf(SelfEntity self) {
                this.self = self;
            }

            public void setHtml(HtmlEntity html) {
                this.html = html;
            }

            public void setIssue(IssueEntity issue) {
                this.issue = issue;
            }

            public void setComments(CommentsEntity comments) {
                this.comments = comments;
            }

            public void setReview_comments(ReviewCommentsEntity review_comments) {
                this.review_comments = review_comments;
            }

            public void setReview_comment(ReviewCommentEntity review_comment) {
                this.review_comment = review_comment;
            }

            public void setCommits(CommitsEntity commits) {
                this.commits = commits;
            }

            public void setStatuses(StatusesEntity statuses) {
                this.statuses = statuses;
            }

            public SelfEntity getSelf() {
                return self;
            }

            public HtmlEntity getHtml() {
                return html;
            }

            public IssueEntity getIssue() {
                return issue;
            }

            public CommentsEntity getComments() {
                return comments;
            }

            public ReviewCommentsEntity getReview_comments() {
                return review_comments;
            }

            public ReviewCommentEntity getReview_comment() {
                return review_comment;
            }

            public CommitsEntity getCommits() {
                return commits;
            }

            public StatusesEntity getStatuses() {
                return statuses;
            }

            public static class SelfEntity {
                private String href;

                public void setHref(String href) {
                    this.href = href;
                }

                public String getHref() {
                    return href;
                }
            }

            public static class HtmlEntity {
                private String href;

                public void setHref(String href) {
                    this.href = href;
                }

                public String getHref() {
                    return href;
                }
            }

            public static class IssueEntity {
                private String href;

                public void setHref(String href) {
                    this.href = href;
                }

                public String getHref() {
                    return href;
                }
            }

            public static class CommentsEntity {
                private String href;

                public void setHref(String href) {
                    this.href = href;
                }

                public String getHref() {
                    return href;
                }
            }

            public static class ReviewCommentsEntity {
                private String href;

                public void setHref(String href) {
                    this.href = href;
                }

                public String getHref() {
                    return href;
                }
            }

            public static class ReviewCommentEntity {
                private String href;

                public void setHref(String href) {
                    this.href = href;
                }

                public String getHref() {
                    return href;
                }
            }

            public static class CommitsEntity {
                private String href;

                public void setHref(String href) {
                    this.href = href;
                }

                public String getHref() {
                    return href;
                }
            }

            public static class StatusesEntity {
                private String href;

                public void setHref(String href) {
                    this.href = href;
                }

                public String getHref() {
                    return href;
                }
            }
        }
    }
}
