package com.example.githubjavapop;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RepositoryAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<RepositoryResponse.ItemsEntity>
            repositoryItems = new ArrayList<RepositoryResponse.ItemsEntity>();

    public RepositoryAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return repositoryItems.size();
    }

    @Override
    public Object getItem(int position) {
        return repositoryItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_repository, parent, false);
        RepositoryResponse.ItemsEntity item = (RepositoryResponse.ItemsEntity) getItem(position);

        //Name
        TextView name = (TextView) rowView.findViewById(R.id.textName);
        name.setText((CharSequence) item.getName());

        //Username
        TextView username = (TextView) rowView.findViewById(R.id.textUsername);
        String usernameAux = item.getOwner().getLogin();
        if(usernameAux.length()>9) {
            usernameAux = usernameAux.substring(0,7) + "...";
        }
        username.setText((CharSequence) usernameAux);

        //Stars
        TextView stars = (TextView) rowView.findViewById(R.id.textStargazersCount);
        stars.setText((CharSequence) Integer.toString(item.getStargazers_count()));

        //Forks
        TextView forks = (TextView) rowView.findViewById(R.id.textForks);
        forks.setText((CharSequence) Integer.toString(item.getForks()));

        //Description
        TextView description = (TextView) rowView.findViewById(R.id.textDescription);
        description.setText((CharSequence) item.getDescription());

        ImageView avatar = (ImageView) rowView.findViewById(R.id.imageAvatar);
        String avatarUrl = item.getOwner().getAvatar_url() + "&s=60";
        Picasso.with(context).setIndicatorsEnabled(false);
        Picasso.with(context).load(avatarUrl).noPlaceholder().into(avatar);

        return rowView;
    }

    public void addItems(ArrayList<RepositoryResponse.ItemsEntity> items) {
        repositoryItems.addAll(items);
    }

    public String getUsername(int position) {
        return repositoryItems.get(position).getOwner().getLogin();
    }

    public String getRepository(int position) {
        return repositoryItems.get(position).getName();
    }
}
