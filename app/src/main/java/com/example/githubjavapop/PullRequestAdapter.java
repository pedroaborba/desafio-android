package com.example.githubjavapop;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PullRequestAdapter extends BaseAdapter{

    private Context context;
    private ArrayList<PullRequestResponse.ItemsEntity>
            pullRequestItems = new ArrayList<PullRequestResponse.ItemsEntity>();

    public PullRequestAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return pullRequestItems.size();
    }

    @Override
    public Object getItem(int position) {
        return pullRequestItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.item_pull_request, parent, false);
        PullRequestResponse.ItemsEntity item = (PullRequestResponse.ItemsEntity) getItem(position);

        //Title
        TextView name = (TextView) rowView.findViewById(R.id.textPrTitle);
        name.setText((CharSequence) item.getTitle());

        //Body
        TextView body = (TextView) rowView.findViewById(R.id.textPrBody);
        body.setText((CharSequence) item.getBody());

        //Username
        TextView username = (TextView) rowView.findViewById(R.id.textPrUsername);
        username.setText((CharSequence) item.getUser().getLogin());

        //Avatar
        ImageView avatar = (ImageView) rowView.findViewById(R.id.imagePrAvatar);
        String avatarUrl = item.getUser().getAvatar_url() + "&s=40";
        Picasso.with(context).setIndicatorsEnabled(false);
        Picasso.with(context).load(avatarUrl).noPlaceholder().into(avatar);

        return rowView;
    }

    public void addItems(ArrayList<PullRequestResponse.ItemsEntity> items) {
        pullRequestItems.addAll(items);
    }

    public String getPullRequestUrl(int position) {
        return pullRequestItems.get(position).getHtml_url();
    }
}
