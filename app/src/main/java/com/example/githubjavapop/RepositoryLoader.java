package com.example.githubjavapop;

import android.content.Context;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import cz.msebera.android.httpclient.Header;

import java.util.ArrayList;

public class RepositoryLoader {

    private RepositoryAdapter repositoryAdapter;
    private RepositoryResponse repositoryResponse;
    private Context context;
    private int currentPage = 1;
    private AsyncHttpClient asyncClient;
    private Gson gson;

    public RepositoryLoader(Context context, RepositoryAdapter repositoryAdapter) {
        this.context = context;
        this.repositoryAdapter = repositoryAdapter;
        gson = new Gson();
        asyncClient = new AsyncHttpClient();
        asyncClient.setUserAgent("Github JavaPop");
    }

    public void loadResponses() {

        String url = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=" + Integer.toString(currentPage);
        currentPage++;

        asyncClient.get(context, url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String responsestr = new String(responseBody);

                repositoryResponse = gson.fromJson(responsestr, RepositoryResponse.class);
                repositoryAdapter.addItems((ArrayList<RepositoryResponse.ItemsEntity>) repositoryResponse.getItems());
                repositoryAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
            }
        });
    }
}
