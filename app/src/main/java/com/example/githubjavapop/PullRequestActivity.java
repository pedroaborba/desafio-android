package com.example.githubjavapop;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;


public class PullRequestActivity extends Activity {

    ListView pullRequestList;
    PullRequestAdapter pullRequestAdapter;
    PullRequestLoader pullRequestLoader;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);

        setTitle("Pull Requests");

        context = this;
        pullRequestAdapter = new PullRequestAdapter(this);
        pullRequestLoader = new PullRequestLoader(this, pullRequestAdapter);
        pullRequestList = (ListView) findViewById(R.id.listPullRequest);
        pullRequestList.setAdapter(pullRequestAdapter);
        String path = getIntent().getStringExtra("username") +
                "/" + getIntent().getStringExtra("repository");
        pullRequestLoader.loadResponses(path);

        pullRequestList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String url = pullRequestAdapter.getPullRequestUrl(position);
                Uri webSite = Uri.parse(url);
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, webSite);
                startActivity(browserIntent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pull_request, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
