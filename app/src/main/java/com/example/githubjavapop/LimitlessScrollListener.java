package com.example.githubjavapop;

import android.util.Log;
import android.widget.AbsListView;
import static com.example.githubjavapop.LimitlessScrollListener.EslState.*;

public abstract class LimitlessScrollListener implements AbsListView.OnScrollListener {

    public static final String LOG_TAG = "EndlessScrollListener";
    private final int visibleThreshold = 15;
    private int previousTotalItemCount = -1;
    public enum EslState {
        IDLE,
        LOADING_DATA
    }
    EslState currentEslState = IDLE;

    @Override
    public void onScroll(
            AbsListView view,
            int firstVisibleItem,
            int visibleItemCount,
            int totalItemCount
    ) {
        switch (currentEslState) {
            case IDLE:
                if(totalItemCount-(firstVisibleItem+visibleItemCount)<visibleThreshold) {
                    currentEslState = LOADING_DATA;
                    onLoadMore();
                }
                break;
            case LOADING_DATA:
                if(previousTotalItemCount<totalItemCount) {
                    previousTotalItemCount = totalItemCount;
                    currentEslState = IDLE;
                }
                break;
        }
    }

    public abstract void onLoadMore();

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }
}
