package com.example.githubjavapop;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Pedro on 12/25/2015.
 */
public class PullRequestLoader {

    private PullRequestAdapter pullRequestAdapter;
    private PullRequestResponse pullRequestResponse;
    private Context context;
    private AsyncHttpClient asyncClient;
    private Gson gson;

    public PullRequestLoader(Context context,PullRequestAdapter pullRequestAdapter) {
        this.context = context;
        this.pullRequestAdapter = pullRequestAdapter;
        gson = new Gson();
        asyncClient = new AsyncHttpClient();
        asyncClient.setUserAgent("Github JavaPop");
    }

    public void loadResponses(String repository) {

        String url = "https://api.github.com/repos/" + repository + "/pulls";

        asyncClient.get(context, url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String responsestr = new String(responseBody);

                responsestr = "{ \"items\": " + responsestr + "}";
                pullRequestResponse = gson.fromJson(responsestr, PullRequestResponse.class);
                pullRequestAdapter.addItems((ArrayList<PullRequestResponse.ItemsEntity>) pullRequestResponse.getItems());
                pullRequestAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                error.printStackTrace();
            }
        });
    }
}
